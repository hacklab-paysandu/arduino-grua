/* Creado: 25-2-2019
 *  
 * Última actualización: 23-8-2019
 *  
 *  Autor: Jorge Rosas
 */
#include <Servo.h>
#include <SoftwareSerial.h>

#define PS_1  2
#define PS_2  3
#define PS_3  5
#define PS_4  4

Servo s_aa1, s_aa2, s_id, s_ac;

void setup ()
{
  Serial.begin( 9600 );

  s_aa1.attach( PS_1 );
  s_aa2.attach( PS_2 );
  s_id.attach( PS_3 );
  s_ac.attach( PS_4 );
}

void loop ()
{
  if ( Serial.available() > 0 )
  {
    char c = Serial.read();
    
    switch ( c )
    {
      case 'a' :
        Serial.println( "adelante" );
        s_aa1.write( 120 );
        s_aa2.write( 130 );
        break;
      case 'b' :
        Serial.println( "atras" );
        s_aa1.write( 20 );
        s_aa2.write( 150 );
        break;
      case 'i' :
        Serial.println( "izquierda" );
        s_id.write( 180 );
        break;
      case 'd' :
        Serial.println( "derecha" );
        s_id.write( 0 );
        break;
      case 'o' :
        Serial.println( "abrir" );
        s_ac.write( 150 );
        break;
      case 'c' :
        Serial.println( "cerrar" );
        s_ac.write( 77 );
        break;
      case 'n' :
        Serial.println( "centrar" );
        centrar_grua();
        break;
    }
  }
}

void centrar_grua ()
{
  s_aa1.write( 55 );
  s_aa2.write( 100 );
  s_id.write( 110 );
}
