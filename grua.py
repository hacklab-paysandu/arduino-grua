from tkinter import *
from tkinter import scrolledtext
import speech_recognition as sr
import threading
import time
import serial
import os

comandos = [
	( 'adelante', 'a' ),
	( 'atrás', 'b' ),
	( 'izquierda', 'i' ),
	( 'derecha', 'd' ),
	( 'centro', 'n' ),
	( 'abrir', 'o' ),
	( 'cerrar', 'c' )
]

mensaje = None
CANT_LOGS = 20

class VozThread ( threading.Thread ) :

	def __init__ ( self, puerto, baud ) :
		super().__init__()
		self.puerto = puerto
		self.baud = baud

	def terminar ( self ) :
		self.detener = True

	def run ( self ) :
		self.rec = sr.Recognizer()
		self.mic = sr.Microphone()

		try :
			grua = serial.Serial( self.puerto, self.baud )
		except Exception :
			self.log( 'Puerto no conectado' )
			return

		self.detener = False
		while not self.detener :
			print( 'Voz' )
			res = self.reconocer()

			if res['exito'] :
				comando = self.get_comando( res['transcripcion'] )

				if comando is not None :
					self.log( f'Se escucho: {res["transcripcion"]}' )
					self.log( f'Enviando: {comando}' )
					grua.write( bytes( comando, 'utf-8' ) )
				else :
					self.log( f'Error: No se reconocieron comandos en "{res["transcripcion"]}"' )
			else :
				self.log( f'Error: {res["error"]}' )

		grua.close()

	def reconocer ( self ) :
		with self.mic as source :
			self.rec.adjust_for_ambient_noise( source )
			self.log( '\nEscuchando...' )
			audio = self.rec.listen( source )

		res = {
			'exito': True,
			'error': None,
			'transcripcion': None
		}

		self.log( 'Interpretando...' )

		try :
			res['transcripcion'] = self.rec.recognize_google( audio, language = 'es-UY' )
		except sr.RequestError :
			res['exito'] = False
			res['error'] = 'API no disponible'
		except sr.UnknownValueError :
			res['exito'] = False
			res['error'] = 'No se reconocieron palabras'

		return res

	def get_comando ( self, res ) :
		for comando in comandos :
			if comando[0] in res :
				return comando[1]

		return None

	def log ( self, txt ) :
		global mensaje
		global ventana

		enviado = False

		while not enviado :
			print( 'enviando...' )
			with lock :
				if ventana is None :
					return
				elif mensaje is not None :
					time.sleep( 0.1 )
				else :
					mensaje = txt
					enviado = True


class Ventana ( Tk ) :

	def __init__ ( self ) :
		super().__init__()
		self.title( 'Grúa por voz' )
		self.resizable( False, False )

		# Se obtienen los puertos conectados a la pc
		self.puertos = self.get_puertos()

		# Se crea la lista para colocar los puertos
		self.lista = Listbox( self )
		self.lista.config( width = 40, height = len( self.puertos ) )
		self.lista.pack()

		# Se llena la lista de puertos
		for p in self.puertos :
			self.lista.insert( END, p )

		# Se crea un campo para ingresar los BAUD para la comunicación
		self.baud = Entry( self, width = 40 )
		# Valor por defecto 9600
		self.baud.delete( 0, END )
		self.baud.insert( 0, '9600' )
		self.baud.pack()

		# Boton de iniciar
		self.boton = Button( self, text = 'Iniciar', command = self.iniciar )
		self.boton.pack()

		# Consola para ir logeando los mensajes
		self.consola = scrolledtext.ScrolledText( self, width = 80, height = CANT_LOGS, state = 'disabled' )
		self.consola.pack()

		self.mensajes = []
		self.loop()

	def iniciar ( self ) :
		global voz

		puerto = self.get_puerto()
		baud = self.get_baud()

		if puerto is None :
			self.log( 'Seleccione un puerto' )
		else :
			self.log( 'Iniciando...' )
			voz = VozThread( puerto, baud )
			voz.start()

	def get_puerto ( self ) :
		if len( self.lista.curselection() ) == 0 :
			return None
		else :
			return self.puertos[self.lista.curselection()[0]].split( ' ' )[0]

	def get_baud ( self ) :
		try :
			return int( self.baud.get() )
		except Exception :
			return 9600

	def log ( self, mensaje ) :
		self.mensajes.extend( mensaje.split( '\n' ) )

		while len( self.mensajes ) >= CANT_LOGS :
			del self.mensajes[:1]

		self.consola.config( state = 'normal' )
		self.consola.delete( '1.0', END )

		for m in self.mensajes :
			self.consola.insert( INSERT, f'\n{m}' )

		self.consola.config( state = 'disabled' )
		self.consola.see( END )

	def loop ( self ) :
		global mensaje

		print( 'loop...' )
		with lock :
			if mensaje is not None :
				self.log( mensaje )

				mensaje = None

		self.after( 1, self.loop )

	def get_puertos ( self ) :
		# Obtiene dispositivos listados como USB
		devs = os.popen( 'find /sys/bus/usb/devices/usb*/ -name dev' ).read()
		ports = []

		# Por cada uno de ellos se chequea si aparece en su directorio /ttyUSB o /ttyACM
		# Dispositivos arduino van a aparecer con una de estas dos
		for d in devs.split( '\n' ) :
			if '/ttyUSB' in d or '/ttyACM' in d :
				# Si un dispositivo tiene de nombre /dev/ttyUSB0, entonces las ultimas
				# dos partes de su ruta en devs será /ttyUSB0/dev

				# Se obtinen las partes
				partes = d.split( '/' )
				# Se colocan en orden inverso
				partes.reverse()
				# Se forma el nombre del puerto en base a las partes de la ruta
				puerto = f'/{partes[0]}/{partes[1]}'

				# Se obtiene informacion sobre el puerto y con grep solamente aparecera la linea que tenga el ID_SERIAL
				id_serial = os.popen( f'udevadm info -q property --export -p {d[:-4]} | grep ID_SERIAL' ).read()
				# Se limpia el formato ID_SERIAL='contenido', para solamente obtener el string del contenido
				# El ID_SERIAL contiene el nombre que se le dio al dispositivo de fabrica, esto ayuda a reconocerlo visualmente
				id_serial = id_serial.replace( 'ID_SERIAL=', '' ).replace( '\'', '' ).replace( '\n', '' )

				# Se agrega el puerto con el formato: puerto (nombre)
				ports.append( f'{puerto} ({id_serial})' )

		# Se devuelven todos los encontrados
		return ports


voz = None
ventana = None
if __name__ == '__main__' :
	lock = threading.Lock()
	ventana = Ventana()
	ventana.mainloop()
	ventana = None

	if voz is not None :
		voz.terminar()